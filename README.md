Cert-exporter

Cert-exporter exposes metrics about the expiry of certificates in kubernetes secret objects.

Usage
=========
Based on a secrets annotation, the cert exporter deployment can identify the expiry of certificate objects. 